from points.Colors import Colors
from points.Point import Point


class Sphere:
    def __init__(self, id_number: int, point: Point, color: Colors, caught: bool = False, solved: bool = False):
        self.id_number = id_number
        self.point = point
        self.color = color
        self.caught = caught
        self.solved = solved

    def get_distance_to(self, other_sphere):
        distance = self.point.get_distance_to(other_point=other_sphere.point)
        # print("Distance from [{0}][{1}] to [{2}],[{3}] is {4}".format(self.point.x, self.point.y,
        #                                                              other_sphere.point.x, other_sphere.point.y,
        #                                                              distance))
        return distance

    def get_distance_to(self, other_point: Point):
        distance = self.point.get_distance_to(other_point=other_point)
        # print("Distance from [{0}][{1}] to [{2}],[{3}] is {4}".format(self.point.x, self.point.y,
        #                                                              other_point.x, other_point.y, distance))
        return distance


