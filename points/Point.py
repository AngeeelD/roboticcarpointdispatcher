import math


class Point:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def get_distance_to(self, other_point):
        leg_a = abs(other_point.x - self.x)
        leg_b = abs(other_point.y - self.y)
        hypotenuse = math.sqrt((pow(leg_a, 2) + pow(leg_b, 2)))
        return hypotenuse
