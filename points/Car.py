from points.Point import Point
import math


class Car:
    def __init__(self, back_point: Point, arm_point: Point):
        self.back_point = back_point
        self.arm_point = arm_point

    def get_car_angle(self):
        return math.degrees(math.atan2(self.arm_point.y - self.back_point.y, self.arm_point.x - self.back_point.x))

    def get_angle_respecting_another_object(self, other_point: Point):
        return math.degrees(math.atan2(other_point.y - self.back_point.y, other_point.x - self.back_point.x))
