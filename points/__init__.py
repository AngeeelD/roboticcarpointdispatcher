from points.Car import Car
from points.Colors import Colors
from points.Point import Point
from points.Sphere import Sphere
from time import sleep
import math

# region definitions
detected_objects = [
    Sphere(
        id_number=1,
        point=Point(
            x=255,
            y=300
        ),
        color=Colors.BLUE),
    Sphere(
        id_number=2,
        point=Point(
            x=155,
            y=240
        ),
        color=Colors.GREEN),
    Sphere(
        id_number=3,
        point=Point(
            x=405,
            y=315
        ),
        color=Colors.RED),
    Sphere(
        id_number=4,
        point=Point(
            x=25,
            y=400
        ),
        color=Colors.BLUE),
    Sphere(
        id_number=5,
        point=Point(
            x=315,
            y=288
        ),
        color=Colors.GREEN),
    Sphere(
        id_number=6,
        point=Point(
            x=335,
            y=508
        ),
        color=Colors.GREEN),
]

car_object = Car(back_point=Point(270, 25), arm_point=Point(300, 25))

RED_AREA = Point(x=150, y=1200)
GREEN_AREA = Point(x=350, y=1200)
BLUE_AREA = Point(x=550, y=1200)


# endregion

# region problem solution
def solve_problem():
    detected_spheres, car = get_detected_objects()
    sorted_sphere = sort_objects(detected_spheres, car_point=car.arm_point)
    solved_items = list(filter(lambda sphere: is_solved(sphere), sorted_sphere))
    while len(sorted_sphere) > len(solved_items):
        detected_spheres, car = get_detected_objects()
        sorted_sphere = sort_objects(detected_spheres, car_point=car.arm_point)
        non_solved_items = list(filter(lambda sphere: not is_solved(sphere), sorted_sphere))
        solve_one_object(car, non_solved_items[0])
        solved_items = list(filter(lambda sphere: is_solved(sphere), sorted_sphere))
    print("Problem solved")
    pass


def solve_one_object(car: Car, sphere: Sphere):
    print("Solving [{0}][{1}] to [{2}][{3}]".format(car.arm_point.x, car.arm_point.y, sphere.point.x, sphere.point.y))

    solved_area = get_central_indicated_area(sphere.color)
    manipulate_robotic_arm(True)
    rotate_car_to(car, sphere.point)
    move_car_forward_to(sphere.point)
    rotate_car_to(car, solved_area)
    move_car_forward_to(solved_area)
    manipulate_robotic_arm(False)
    move_car_backward()
    return True


# endregion

# region methods

# region validations
def is_solved(sphere: Sphere):
    solved_area = get_area_bounds(color=sphere.color)
    return (sphere.point.x >= solved_area["left_top_corner"].x and sphere.point.x >= solved_area[
        "left_bottom_corner"].x and
            sphere.point.x <= solved_area["right_top_corner"].x and sphere.point.x <= solved_area[
                "right_bottom_corner"].x and
            sphere.point.y >= solved_area["right_bottom_corner"].y and sphere.point.y <= solved_area[
                "right_bottom_corner"].y and
            sphere.point.y <= solved_area["left_top_corner"].y and sphere.point.y <= solved_area["right_top_corner"].y)


# endregion

# region getters methods
def get_detected_objects():
    # TODO: Replace this static return with Jaime implementation
    return detected_objects, car_object


def get_central_indicated_area(color: Colors):
    switcher = {
        Colors.RED: RED_AREA,
        Colors.GREEN: GREEN_AREA,
        Colors.BLUE: BLUE_AREA
    }
    return switcher[color]


def get_area_bounds(color: Colors, horizontal_padding=150, vertical_padding=200):
    indicated_area = get_central_indicated_area(color=color)
    return {
        "left_top_corner": Point(indicated_area.x - horizontal_padding, indicated_area.y + vertical_padding),
        "right_top_corner": Point(indicated_area.x + horizontal_padding, indicated_area.y + vertical_padding),
        "right_bottom_corner": Point(indicated_area.x + horizontal_padding, indicated_area.y - vertical_padding),
        "left_bottom_corner": Point(indicated_area.x - horizontal_padding, indicated_area.y - vertical_padding),
    }


# endregion

# region Point Objects
def sort_objects(objects: [], car_point: Point):
    return sorted(objects, key=lambda obj: obj.get_distance_to(other_point=car_point))


# endregion

# region robot methods


def rotate_car_to(car: Car, point: Point):
    car_angle = car.get_car_angle()
    object_angle = car.get_angle_respecting_another_object(point)
    degrees_diff = object_angle - car_angle
    bt_rotate_car(degrees_diff)
    pass


def move_car_forward_to(point: Point, padding_accurate=5):
    detected_spheres, car = get_detected_objects()
    while (car.arm_point.y < point.y - padding_accurate
           or car.arm_point.y > point.y + padding_accurate) \
            and (car.arm_point.x < point.x - padding_accurate
                 or car.arm_point.x > point.x + padding_accurate):
        # TODO: call instruction to rotate the car using BT
        pass
    pass


def move_car_backward(instruction_time=1000, stoppable=True):
    # TODO: call instruction to move backward the car
    sleep(instruction_time)
    if stoppable:
        # TODO: call instruction to stop the car using BT
        pass
    pass


def manipulate_robotic_arm(open_arm: bool = False):
    if open_arm:
        # TODO: instruction to the robotic open arm
        pass
    else:
        # TODO: instruction to the robotic open arm
        pass
    simulate_manipulate_robotic_arm(open_arm)
    sleep(1500)
    pass


def bt_rotate_car(degrees_to_rotate):
    # TODO: call instruction to rotate using BT
    delay_time = (1252 * abs(degrees_to_rotate)) / 360
    sleep(delay_time + 1000)
    pass


# endregion

# region simulations
def simulate_manipulate_robotic_arm(open_arm: bool = False):
    if open_arm:
        print("Opening robotic arm")
        pass
    else:
        print("Closing robotic arm")
        pass


# endregion

# endregion

# region main
def main():
    solve_problem()
    pass


if __name__ == "__main__":
    main()
# endregion
