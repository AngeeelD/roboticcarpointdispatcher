from enum import Enum


class Colors(Enum):
    RED = 1
    GREEN = 2
    BLUE = 3
